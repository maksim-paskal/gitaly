syntax = "proto3";

package gitaly;

import "lint.proto";

option go_package = "gitlab.com/gitlab-org/gitaly/v16/proto/go/gitalypb";

// NamespaceService is a service which provides RPCs to manage namespaces of a
// storage. Namespaces had been used before Gitaly migrated to hashed storages
// and shouldn't be used nowadays anymore.
service NamespaceService {

  // AddNamespace ...
  rpc AddNamespace(AddNamespaceRequest) returns (AddNamespaceResponse) {
    option (op_type) = {
      op: MUTATOR
      scope_level: STORAGE,
    };
  }

  // RemoveNamespace ...
  rpc RemoveNamespace(RemoveNamespaceRequest) returns (RemoveNamespaceResponse) {
    option (op_type) = {
      op: MUTATOR
      scope_level: STORAGE,
    };
  }

  // RenameNamespace ...
  rpc RenameNamespace(RenameNamespaceRequest) returns (RenameNamespaceResponse) {
    option (op_type) = {
      op: MUTATOR
      scope_level: STORAGE,
    };
  }

  // NamespaceExists ...
  rpc NamespaceExists(NamespaceExistsRequest) returns (NamespaceExistsResponse) {
    option (op_type) = {
      op: ACCESSOR
      scope_level: STORAGE,
    };
  }

}

// AddNamespaceRequest ...
message AddNamespaceRequest {
  // storage_name ...
  string storage_name = 1 [(storage)=true];
  // name ...
  string name = 2;
}

// RemoveNamespaceRequest ...
message RemoveNamespaceRequest {
  // storage_name ...
  string storage_name = 1 [(storage)=true];
  // name ...
  string name = 2;
}

// RenameNamespaceRequest ...
message RenameNamespaceRequest {
  // storage_name ...
  string storage_name = 1 [(storage)=true];
  // from ...
  string from = 2;
  // to ...
  string to = 3;
}

// NamespaceExistsRequest ...
message NamespaceExistsRequest {
  // storage_name ...
  string storage_name = 1 [(storage)=true];
  // name ...
  string name = 2;
}

// NamespaceExistsResponse ...
message NamespaceExistsResponse {
  // exists ...
  bool exists = 1;
}

// AddNamespaceResponse ...
message AddNamespaceResponse {
}

// RemoveNamespaceResponse ...
message RemoveNamespaceResponse {
}

// RenameNamespaceResponse ...
message RenameNamespaceResponse {
}
